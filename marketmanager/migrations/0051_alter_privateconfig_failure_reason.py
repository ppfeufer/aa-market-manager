# Generated by Django 4.2.6 on 2023-11-03 13:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marketmanager', '0050_alter_managedsupplyconfig_managed_channels_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='privateconfig',
            name='failure_reason',
            field=models.CharField(blank=True, default='Failure Reason', max_length=100, verbose_name='Failure Reason'),
        ),
    ]
