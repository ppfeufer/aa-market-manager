# Generated by Django 4.0.3 on 2022-03-17 14:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marketmanager', '0016_alter_typestatistics_eve_region'),
    ]

    operations = [
        migrations.AlterField(
            model_name='watchconfig',
            name='managed_app',
            field=models.CharField(blank=True, default='', help_text='The App managing this WatchConfig', max_length=50, verbose_name='Managed By App'),
        ),
        migrations.AlterField(
            model_name='watchconfig',
            name='managed_app_identifier',
            field=models.CharField(blank=True, default='', help_text='An identifier relevant to the App managing this WatchConfig', max_length=50, verbose_name='Managed App-Identifier'),
        ),
        migrations.AlterField(
            model_name='watchconfig',
            name='manged_app_reason',
            field=models.CharField(blank=True, default='', help_text='User Facing reason for this Managed WatchConfig', max_length=50, verbose_name='Managed App Reason'),
        ),
    ]
