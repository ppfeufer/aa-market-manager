# Generated by Django 4.0.6 on 2022-08-06 06:37

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marketmanager', '0033_alter_managedwatchconfig_managed_jita_compare_percent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='watchconfig',
            name='managed_watch_config',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='marketmanager.managedwatchconfig', verbose_name='Managed Watch Configs'),
        ),
    ]
