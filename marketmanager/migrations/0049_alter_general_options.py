# Generated by Django 4.0.10 on 2023-10-25 13:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('marketmanager', '0048_alter_channel_options_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='general',
            options={'default_permissions': (), 'managed': False, 'permissions': (('basic_market_browser', 'Can access the Standard Market Browser'), ('basic_market_watches', 'Can view all SupplyConfigs and their status'), ('order_highlight_user', "Market orders owned by the user's characters may be highlighted in the standard/basic Market Browser"), ('order_highlight_corporation', 'Market orders owned by any corporation a user is a member of may be highlighted in the standard/basic Market Browser WARNING: This has no checks for Corporation Roles.'), ('advanced_market_browser', 'Can access the Advanced Market Browser'), ('can_add_token_character', 'Can add a Character Token with required scopes'), ('can_add_token_corporation', 'Can add a Corpration Token with required scopes'))},
        ),
    ]
